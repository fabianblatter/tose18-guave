package ch.briggen.bfh.sparklist.domain;




	import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

	import java.sql.Connection;
	import java.sql.PreparedStatement;
	import java.sql.ResultSet;
	import java.sql.SQLException;
	import java.util.Collection;
	import java.util.LinkedList;

	import org.slf4j.Logger;
	import org.slf4j.LoggerFactory;


	/**
	 * Repository für alle Stakeholder. 
	 * Hier werden alle Funktionen für die DB-Operationen zu Stakeholdern implementiert
	 * @author Guave
	 *
	 */


	public class StakeholderRepository {
		
		private final Logger log = LoggerFactory.getLogger(StakeholderRepository.class);
		

		/**
		 * Liefert alle Stakeholder in der Datenbank
		 * @return Collection aller Stakeholder
		 */
		public Collection<Stakeholder> getAll()  {
			log.trace("getAll");
			try(Connection conn = getConnection())
			{
				PreparedStatement stmt = conn.prepareStatement("select id, stakeholder, name, vorname, email, einfluss, interesse from Stakeholder");
				ResultSet rs = stmt.executeQuery();
				return mapStakeholder(rs);		
			}
			catch(SQLException e)
			{
				String msg = "SQL error while retreiving all stakeholder. ";
				log.error(msg, e);
				throw new RepositoryException(msg);
			}
		}

		/**
		 * Liefert alle Stakeholder mit dem angegebenen Namen
		 * @param name
		 * @return Collection mit dem Namen "name"
		 */
		public Collection<Stakeholder> getByName(String name) {
			log.trace("getByName " + name);
			
			try(Connection conn = getConnection())
			{
				PreparedStatement stmt = conn.prepareStatement("select id, stakeholder, name, vorname, email, einfluss, interesse from Stakeholder where name=?");
				stmt.setString(1, name);
				ResultSet rs = stmt.executeQuery();
				return mapStakeholder(rs);		
			}
			catch(SQLException e)
			{
				String msg = "SQL error while retreiving stakeholders by name " + name;
				log.error(msg, e);
				throw new RepositoryException(msg);
			}
				
		}

		/**
		 * Liefert das Stakeholder mit der übergebenen Id
		 * @param id id des Stakeholders
		 * @return Stakeholder oder NULL
		 */
		public Stakeholder getById(long id) {
			log.trace("getById " + id);
			
			//TODO: There is an issue with this repository method. Find and fix it!
			try(Connection conn = getConnection())
			{
				PreparedStatement stmt = conn.prepareStatement("select id, stakeholder, name, vorname, email, einfluss, interesse from Stakeholder where id=?");
				stmt.setLong(1, id);
				ResultSet rs = stmt.executeQuery();
				return mapStakeholder(rs).iterator().next();		
			}
			catch(SQLException e)
			{
				String msg = "SQL error while retreiving stakeholder by id " + id;
				log.error(msg, e);
				throw new RepositoryException(msg);
			}
						
		}

		/**
		 * Speichert das übergebene Stakeholder in der Datenbank. UPDATE.
		 * @param i
		 */
		public void save(Stakeholder i) {
			log.trace("save " + i);
			
			try(Connection conn = getConnection())
			{
				PreparedStatement stmt = conn.prepareStatement("update Stakeholder set id=?, stakeholder=?, name=?, vorname=?, email=?, einfluss=?, interesse=? where id=?");
				stmt.setLong(1, i.getId());
				stmt.setString(2, i.getStakeholder());
				stmt.setString(3, i.getName());
				stmt.setString(4, i.getVorname());
				stmt.setString(5, i.getEmail());
				stmt.setLong(6, i.getEinfluss());
				stmt.setLong(7, i.getInteresse());
				stmt.executeUpdate();
			}
			catch(SQLException e)
			{
				String msg = "SQL error while updating stakeholder " + i;
				log.error(msg , e);
				throw new RepositoryException(msg);
			}
			
		}

		/**
		 * Löscht das Stakeholder mit der angegebenen Id von der DB
		 * @param id Stakeholder ID
		 */
		public void delete(long id) {
			log.trace("delete " + id);
			
			try(Connection conn = getConnection())
			{
				PreparedStatement stmt = conn.prepareStatement("delete from stakeholder where id=?");
				stmt.setLong(1, id);
				stmt.executeUpdate();
			}
			catch(SQLException e)
			{
				String msg = "SQL error while deleteing stakehodlder by id " + id;
				log.error(msg, e);
				throw new RepositoryException(msg);
			}
						

		}

		/**
		 * Speichert das angegebene Stakeholder in der DB. INSERT.
		 * @param i neu zu erstellendes Stakeholder
		 * @return Liefert die von der DB generierte id des neuen Stakeholders zurück
		 */
		public long insert(Stakeholder i) {
			
			log.trace("insert " + i);

			//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
			
			try(Connection conn = getConnection())
			{
				PreparedStatement stmt = conn.prepareStatement("insert into stakeholder (stakeholder, name, vorname, email, einfluss, interesse) values (?,?,?,?,?,?)");
				stmt.setString(1, i.getStakeholder());
				stmt.setString(2, i.getName());
				stmt.setString(3, i.getVorname());
				stmt.setString(4, i.getEmail());
				stmt.setLong(5, i.getEinfluss());
				stmt.setLong(6, i.getInteresse());
				stmt.executeUpdate();
				ResultSet key = stmt.getGeneratedKeys();
				key.next();
				Long id = key.getLong(1);
				return id;
			}
			catch(SQLException e)
			{
				String msg = "SQL error while updating stakeholder " + i;
				log.error(msg , e);
				throw new RepositoryException(msg);
			}

		}
		
		/**
		 * Helper zum konvertieren der Resultsets in Stakeholder-Objekte. Siehe getByXXX Methoden.
		 * @author Guave
		 * @throws SQLException 
		 *
		 */
		private static Collection<Stakeholder> mapStakeholder(ResultSet rs) throws SQLException 
		{
			LinkedList<Stakeholder> stakeholderlist = new LinkedList<Stakeholder>();
			while(rs.next())
			{
				Stakeholder i = new Stakeholder(rs.getLong("id"), rs.getString("stakeholder"), rs.getString("name"), rs.getString("vorname"), rs.getString("email"), rs.getLong("einfluss"), rs.getLong("interesse"));
				stakeholderlist.add(i);
			}
			return stakeholderlist;
		}

	}

