package ch.briggen.bfh.sparklist.domain;

import java.util.ArrayList;
import java.util.List;

public class Project {



	/**
	 * 
	 * @author Guave
	 *
	 */

		
		private long id;
		private String name;
		private boolean IsActive;
		private String Owner; //ev ID einer Person statt String
		private long Budget;
		private long PlannedValue;
		private long ActualCosts;
		private String Milestone;
		private String Description;
		private long risk;
		private List<Long> stakeholderIds = new ArrayList<>();
		private List<Long> employeeIds = new ArrayList<>();
		private int phaseId;
		
		
		/**
		 * Defaultkonstruktor für die Verwendung in einem Controller
		 */
		public Project()
		{
			//Default Werte werden gesetzt
			this.IsActive = true;
			this.Budget = 1;
			this.PlannedValue = 1;
			this.ActualCosts = 1;
			this.Milestone = "";
			this.Description = "";	
			this.phaseId = 0;
		}
		
		/**
		 * Konstruktor
		 * @param id Eindeutige Id
		 * @param name Namen
		 * @param IsActive Status
		 * @param Owner Eigentümer
		 * @param Budget Projektbudget
		 * @param PlannedValue Geplanter Wer
		 * @param Milestone Meilensteine
		 * @param risk Risikograd
		 * @param stakeholderIds Stakholder-ids
		 * @param employeeIds Employee-Ids
		 * @param Description Beschreibung
		 * @param phaseId Phasen Id
		 */
		public Project(long id, String name, boolean IsActive, String Owner, long Budget, long PlannedValue, long ActualCosts, String Milestone, String Description, long risk, List<Long> stakeholderIds, List<Long> employeeIds, int phaseId)
		{
			this.id = id;
			this.name = name;
			this.IsActive = IsActive;
			this.Owner = Owner;
			this.Budget = Budget;
			this.PlannedValue = PlannedValue ;
			this.ActualCosts = ActualCosts;
			this.Milestone = Milestone;
			this.risk = risk;
			this.stakeholderIds = stakeholderIds;
			this.employeeIds = employeeIds;
			this.Description = Description;
			this.phaseId = phaseId;
		}
	
		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}


		public long getId() {
			return id;
		}
		
		public void setId(long id) {
			this.id = id;
		}
		
		

		public String getDescription() {
			return Description;
		}

		public void setDescription(String description) {
			this.Description = description;
		}

		public List<Long> getStakeholderIds() {
			return stakeholderIds;
		}

		public void setStakeholderIds(List<Long> stakeholderIds) {
			this.stakeholderIds = stakeholderIds;
		}
		
		public List<Long> getEmployeeIds() {
			return employeeIds;
		}

		public void setEmployeeIds(List<Long> employeeIds) {
			this.employeeIds = employeeIds;
		}
		

		public String getMilestone() {
			return Milestone;
		}

		public void setMilestone(String milestone) {	
			this.Milestone = milestone;
		}

		public long getActualCosts() {
			return ActualCosts;
		}

		public void setActualCosts(int actualCosts) {
			this.ActualCosts = actualCosts;
		}

		public long getPlannedValue() {
			return PlannedValue;
		}

		public void setPlannedValue(int plannedValue) {
			this.PlannedValue = plannedValue;
		}

		public long getBudget() {
			return Budget;
		}

		public void setBudget(int budget) {
			this.Budget = budget;
		}

		public String getOwner() {
			return Owner;
		}

		public void setOwner(String owner) {
			this.Owner = owner;
		}

		public boolean getIsActive() {
			return IsActive;
		}

		public void setIsActive(boolean isActive) {
			IsActive = isActive;
		}
		
		public long getRisk() {
			return risk;
		}

		public void setRisk(long risk) {
			this.risk = risk;
		}
		
		public int getPhaseId() {
			return phaseId;
		}

		public void setPhaseId(int phaseId) {
			this.phaseId = phaseId;
		}
		
		public void nextPhase() {
			if (phaseId < 3) {
				phaseId++;
			}
		}
		
		
		@Override
		public String toString() {
			return String.format("Item:{id: %d; name: %s; IsActive: %s; Owner: %s; Budget: %d; PlannedValue: %d; ActualCosts: %d; Milestone: %s; Description: %s}", id, name, IsActive, Owner, Budget, PlannedValue, ActualCosts, Milestone, Description);
		}
		

	}

