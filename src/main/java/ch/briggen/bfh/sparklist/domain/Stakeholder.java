package ch.briggen.bfh.sparklist.domain;

public class Stakeholder {



	/**
	 * 
	 * @author Guave
	 *
	 */

		
		private long id;
		private String stakeholder;
		private String name;
		private String vorname;
		private String email;
		private long einfluss;
		private long interesse;
		
		
		/**
		 * Defaultkonstruktor für die Verwendung in einem Controller
		 */
		public Stakeholder()
		{
			
			//Default Werte werden gesetzt
			this.stakeholder = " ";
			this.name = " ";
			this.vorname = " ";
			this.email = " ";
			this.einfluss = 1;
			this.interesse = 1;
			
			
		}
		
		/**
		 * Konstruktor
		 * @param id Eindeutige Id
		 * @param stakeholder Stakeholdernamen
		 * @param name Namen
		 * @param vorname Vorname des Stakeholder
		 * @param email E-Mail-Adresse
		 * @param einfluss Einfluss
		 * @param interesse Interesse
		 */
		public Stakeholder(long id, String stakeholder, String name, String vorname, String email, long einfluss, long interesse )
		{
			this.id = id;
			this.stakeholder = stakeholder;
			this.name = name;
			this.vorname = vorname;
			this.email = email;
			this.einfluss = einfluss;
			this.interesse = interesse;
		}	
	
		public long getId() {
			return id;
		}
		
		public void setId(long id) {
			this.id = id;
		}
		
		public String getStakeholder() {
			return stakeholder;
		}

		public void setStakeholder(String stakeholder) {
			this.stakeholder = stakeholder;
		}
		
		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getVorname() {
			return vorname;
		}

		public void setVorname(String vorname) {
			this.vorname = vorname;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public long getEinfluss() {
			return einfluss;
		}

		public void setEinfluss(long einfluss) {
			this.einfluss = einfluss;
		}

		public long getInteresse() {
			return interesse;
		}

		public void setInteresse(long interesse) {
			this.interesse = interesse;
		}
		
		public String fullName() {
			return this.name + this.vorname;
		}
		
		@Override
		public String toString() {
			return String.format("Item:{id: %d; stakeholder: %s; name: %s; vorname: %s; email: %s; einfluss: %d; interesse: %d;}", id, stakeholder, name, vorname, email, einfluss, interesse );
			
			  
			
		}
		

	}

