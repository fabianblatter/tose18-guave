package ch.briggen.bfh.sparklist.domain;

import java.util.ArrayList;
import java.util.List;

public class Portfolio {

	/**
	 * 
	 * @author Guave
	 *
	 */

		
		private long id;
		private String name;
		private boolean isActive;
		private String Description;
		private List<Long> projectIds = new ArrayList<>();
		
		
		/**
		 * Konstruktor
		 * @param id Eindeutige Id
		 * @param name Name 
		 * @param quantity Menge
		 * @param isActive Aktivity Status
		 * @param Description Beschreibung
		 * @param projectIds = Id der Projekte;
		 * 
		 * 
		 * 
		 */
		public Portfolio(long id, String name, boolean isActive, String Description, List<Long> projectIds) {
			this.id = id;
			this.name = name;
			this.isActive = isActive;
			this.Description = Description;
			this.projectIds = projectIds;
		}
		
	
		public Portfolio() {
			
		}


		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public boolean getIsActive() {
			return isActive;
		}

		public void setIsActive(boolean isActive) {
			this.isActive = isActive;
		}

		public String getDescription() {
			return Description;
		}

		public void setDescription(String description) {
			Description = description;
		}


		public List<Long> getProjectIds() {
			return projectIds;
		}


		public void setProjectIds(List<Long> projectIds) {
			this.projectIds = projectIds;
		}

		@Override
		public String toString() {
			String projectsString = "";
			ProjectRepository projectRepo = new ProjectRepository();
//			for (int i = 0; i < projectIds.size(); i++) {
//				Project j = projectRepo.getById(projectIds.get(i));
//				projectsString += j.getName() + "/n";
//			}
			return String.format("Item:{id: %d; name: %s; IsActive: %s; Description: %s; Projects: %s}", id, name, isActive, Description, projectsString);
		}
	}

