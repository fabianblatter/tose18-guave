
package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
	 * Repository für alle Projekte. 
	 * Hier werden alle Funktionen für die DB-Operationen zu Projects implementiert
	 * @author Guave
	 *
	 */


	public class ProjectRepository {
		
		private final Logger log = LoggerFactory.getLogger(ProjectRepository.class);
		private static Map<Long, ArrayList<Long>> stakeholderProjects;
		private static Map<Long, ArrayList<Long>> employeesProjects;
			
		public ProjectRepository() {
			stakeholderProjects = getStakeholderIds();
			employeesProjects = getEmployeeIds();
		}
		

		/**
		 * Liefert alle Projects in der Datenbank
		 * @return Collection aller Projects
		 */
		public Collection<Project> getAll()  {
			log.trace("getAll");
			try(Connection conn = getConnection())
			{
				PreparedStatement stmt = conn.prepareStatement("select * from project");
				ResultSet rs = stmt.executeQuery();
				return mapProject(rs);		
			}
			catch(SQLException e)
			{
				String msg = "SQL error while retreiving all items. ";
				log.error(msg, e);
				throw new RepositoryException(msg);
			}
		}

		/**
		 * Liefert das Project mit der übergebenen Id
		 * @param id id des Projects
		 * @return Projects oder NULL
		 */
		public Project getById(long id) {
			log.trace("getById " + id);
			
			//Fehler in Methode
			try(Connection conn = getConnection())
			{
				PreparedStatement stmt = conn.prepareStatement("select id, name, isactive, owner, budget, plannedvalue, actualcosts , milestone, description, risk, phaseId from project where id=?");
				System.out.print(stmt);
				stmt.setLong(1, id);
				ResultSet rs = stmt.executeQuery();
				return mapProject(rs).iterator().next();		
			}
			catch(SQLException e)
			{
				String msg = "SQL error while retreiving project by id " + id;
				log.error(msg, e);
				throw new RepositoryException(msg);
			}
						
		}
		
		 public int countRows(Connection conn, String tableName) throws SQLException {
			    // select the number of rows in the table
			  PreparedStatement stmt = null;
			    ResultSet rs = null;
			    int rowCount = -1;
			    try {
			      
			     PreparedStatement stmt1 = conn.prepareStatement("SELECT COUNT(*) FROM PROJECT ");
			      ResultSet rs1 = stmt1.executeQuery();
			      // get the number of rows from the result set
			      
			      rs1.next();
			      rowCount = rs1.getInt(1);
			    } finally {
			      rs.close();
			      stmt.close();
			    }
			    return rowCount;
			  }


		/**
		 * Speichert das übergebene Project in der Datenbank. UPDATE.
		 * @param i
		 */
		public void save(Project project) {
			log.trace("update " + project);
			
			try(Connection conn = getConnection())
			{
				PreparedStatement stmt = conn.prepareStatement("update project set name=?, isactive=?, owner=?, budget=?, plannedvalue=?, actualcosts=?, milestone=?, description=?, risk=?, phaseId=? where id=?");
				stmt.setString(1, project.getName());
				stmt.setBoolean(2, project.getIsActive());
				stmt.setString(3, project.getOwner());
				stmt.setLong(4, project.getBudget());
				stmt.setLong(5, project.getPlannedValue());
				stmt.setLong(6, project.getActualCosts());
				stmt.setString(7, project.getMilestone());
				stmt.setString(8, project.getDescription());
				stmt.setLong(9, project.getRisk());
				stmt.setInt(10, project.getPhaseId());
				stmt.setLong(11, project.getId());
				stmt.executeUpdate();
				
				
				stmt = conn.prepareStatement("delete from stakeholder_projects where project_id=?");	
				stmt.setLong(1, project.getId());				
				stmt.executeUpdate();	
				
				List<Long> stakeholderIds = project.getStakeholderIds();
				
				for (int i = 0; i < stakeholderIds.size(); i++) {
					stmt = conn.prepareStatement("insert into stakeholder_projects (stakeholder_id, project_id) values (?, ?)");	
					stmt.setLong(1, stakeholderIds.get(i));
					stmt.setLong(2, project.getId());	
					stmt.executeUpdate();	
				}
				
				stmt = conn.prepareStatement("delete from employee_projects where project_id=?");	
				stmt.setLong(1, project.getId());				
				stmt.executeUpdate();	
				
				List<Long> employeeIds = project.getEmployeeIds();
				
				for (int i = 0; i < employeeIds.size(); i++) {
					stmt = conn.prepareStatement("insert into employee_projects (employee_id, project_id) values (?, ?)");	
					stmt.setLong(1, employeeIds.get(i));				
					stmt.setLong(2, project.getId());
					stmt.executeUpdate();	
				}
			}
			catch(SQLException e)
			{
				String msg = "SQL error while updating item " + project;
				log.error(msg , e);
				throw new RepositoryException(msg);
			}
			
			
		}

		/**
		 * Löscht das Project mit der angegebenen Id von der DB
		 * @param id Project ID
		 */
		public void delete(long id) {
			log.trace("delete " + id);
			
			try(Connection conn = getConnection())
			{
				PreparedStatement stmt = conn.prepareStatement("delete from project where id=?");
				stmt.setLong(1, id);
				stmt.executeUpdate();
			}
			catch(SQLException e)
			{
				String msg = "SQL error while deleteing items by id " + id;
				log.error(msg, e);
				throw new RepositoryException(msg);
			}
					
		}

		/**
		 * Speichert das angegebene Project in der DB. INSERT.
		 * @param i neu zu erstellendes Project
		 * @return Liefert die von der DB generierte id des neuen Project zurück
		 */
		public long insert(Project project) {
			log.trace("insert " + project);

			//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
			
			try(Connection conn = getConnection())
			{
				PreparedStatement stmt = conn.prepareStatement("insert into project (name, isactive, owner, budget, plannedvalue, actualcosts, milestone, risk, description, phaseId) values (?,?,?,?,?,?,?,?,?,?)");
				stmt.setString(1, project.getName());
				stmt.setBoolean(2, project.getIsActive());
				stmt.setString(3, project.getOwner());
				stmt.setLong(4, project.getBudget());
				stmt.setLong(5, project.getPlannedValue());
				stmt.setLong(6, project.getActualCosts());
				stmt.setString(7, project.getMilestone());
				stmt.setLong(8, project.getRisk());
				stmt.setString(9, project.getDescription());
				stmt.setInt(10, project.getPhaseId());
				stmt.executeUpdate();

				ResultSet key = stmt.getGeneratedKeys();
				key.next();
				Long pid = key.getLong(1);
				
				List<Long> stakeholderIds = project.getStakeholderIds();
				
				for (int i = 0; i < stakeholderIds.size(); i++) {
					stmt = conn.prepareStatement("insert into stakeholder_projects (stakeholder_id, project_id) values (?, ?)");	
					stmt.setLong(1, stakeholderIds.get(i));
					stmt.setLong(2, pid);	
					stmt.executeUpdate();	
				}
				
				List<Long> employeeIds = project.getEmployeeIds();
				
				for (int i = 0; i < employeeIds.size(); i++) {
					stmt = conn.prepareStatement("insert into employee_projects (employee_id, project_id) values (?, ?)");				
					stmt.setLong(1, employeeIds.get(i));
					stmt.setLong(2, pid);	
					stmt.executeUpdate();	
				}
				
				return pid;
				
			}
			catch(SQLException e)
			{
				String msg = "SQL error while updating item " + project;
				log.error(msg , e);
				throw new RepositoryException(msg);
			}
		}
		
		/**
		 * Helper zum konvertieren der Resultsets in Project-Objekte. Siehe getByXXX Methoden.
		 * @author Guave
		 * @throws SQLException 
		 *
		 */
		private static Collection<Project> mapProject(ResultSet rs) throws SQLException {
			LinkedList<Project> listProject = new LinkedList<Project>();
			while(rs.next())
			{
				long pid = rs.getLong("id");
				ArrayList<Long> stakeholderIds = getStakeholders(pid);
				ArrayList<Long> employeeIds = getEmployees(pid);
				boolean a = rs.getBoolean("isactive");
				Project i = new Project(
						pid,
						rs.getString("name"),
						rs.getBoolean("isactive"),
						rs.getString("owner"), 
						rs.getLong("budget"), 
						rs.getLong("plannedvalue"), 
						rs.getLong("actualcosts"), 
						rs.getString("milestone"), 
						rs.getString("description"),
						rs.getLong("risk"), 
						stakeholderIds,
						employeeIds, 
						rs.getInt("phaseId")
				);
				
				
				listProject.add(i);
			}
			return listProject;
		}
		
		private static int countProjects(LinkedList listproject){
			int countproject = listproject.size();
			return countproject;
			 
			} 
		
		
		//Get Stakeholders
		private static ArrayList<Long> getStakeholders(long projectId) {	
			stakeholderProjects = getStakeholderIds();
			if (stakeholderProjects.containsKey(projectId)) {
				return stakeholderProjects.get(projectId);
			} else {
				return new ArrayList<Long>();
			}
		}
		//Get Employees
		private static ArrayList<Long> getEmployees(long projectId) {
			employeesProjects = getEmployeeIds();
			if (employeesProjects.containsKey(projectId)) {
				return employeesProjects.get(projectId);
			} else {
				return new ArrayList<Long>();
			}
		}

		//Stakeholder
		private static Map<Long, ArrayList<Long>> getStakeholderIds() {
			try(Connection conn = getConnection()) {
				PreparedStatement stmt = conn.prepareStatement("select * "
						+ "from stakeholder_projects");
				ResultSet ps = stmt.executeQuery();
				
				Map<Long, ArrayList<Long>> stakeholderIds = new HashMap<>();
				while(ps.next()) {
					Long sid = ps.getLong("project_id");
					ArrayList<Long> stakeholders = stakeholderIds.get(sid);
					
					if (stakeholders == null) {
						stakeholders = new ArrayList<Long>();
					}
					stakeholders.add(ps.getLong("stakeholder_id"));
					stakeholderIds.put(sid, stakeholders);
				}
				return stakeholderIds;
			} catch(SQLException e) {
				String msg = "SQL error while getting projectIds";
				throw new RepositoryException(msg);
			}
		}
		
		
		//Employee
		private static Map<Long, ArrayList<Long>> getEmployeeIds() {
			try(Connection conn = getConnection()) {
				PreparedStatement stmt = conn.prepareStatement("select * "
						+ "from employee_projects");
				ResultSet pe = stmt.executeQuery();
				
				Map<Long, ArrayList<Long>> employeeIds = new HashMap<>();
				while(pe.next()) {
					Long eid = pe.getLong("project_id");
					ArrayList<Long> employees = employeeIds.get(eid);
					
					if (employees == null) {
						employees = new ArrayList<Long>();
					}
					employees.add(pe.getLong("employee_id"));
					employeeIds.put(eid, employees);
				}
				return employeeIds;
			} catch(SQLException e) {
				String msg = "SQL error while getting projectIds";
				throw new RepositoryException(msg);
			}
		}

	}
