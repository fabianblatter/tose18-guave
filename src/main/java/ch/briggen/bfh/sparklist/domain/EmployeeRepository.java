	package ch.briggen.bfh.sparklist.domain;

	import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

	import java.sql.Connection;
	import java.sql.PreparedStatement;
	import java.sql.ResultSet;
	import java.sql.SQLException;
	import java.util.Collection;
	import java.util.LinkedList;

	import org.slf4j.Logger;
	import org.slf4j.LoggerFactory;

public class EmployeeRepository {


		
		private final Logger log = LoggerFactory.getLogger(ItemRepository.class);
		

		/**
		 * Liefert alle employees in der Datenbank
		 * @return Collection aller Employees
		 */
		public Collection<Employee> getAll()  {
			log.trace("getAll");
			try(Connection conn = getConnection())
			{
				PreparedStatement stmt = conn.prepareStatement("select id, name, firstname, function, salary from employee");
				ResultSet rs = stmt.executeQuery();
				return mapEmployee(rs);		
			}
			catch(SQLException e)
			{
				String msg = "SQL error while retreiving all items. ";
				log.error(msg, e);
				throw new RepositoryException(msg);
			}
		}

		/**
		 * Liefert alle Employee mit dem angegebenen Namen
		 * @param name
		 * @return Collection mit dem Namen "name"
		 */
		public Collection<Employee> getByName(String name) {
			log.trace("getByName " + name);
			
			try(Connection conn = getConnection())
			{
				PreparedStatement stmt = conn.prepareStatement("select id, name, firstname, function, salary from employee where name=?");
				stmt.setString(1, name);
				ResultSet rs = stmt.executeQuery();
				return mapEmployee(rs);		
			}
			catch(SQLException e)
			{
				String msg = "SQL error while retreiving employee by name " + name;
				log.error(msg, e);
				throw new RepositoryException(msg);
			}
				
		}

		/**
		 * Liefert das Employee mit der übergebenen Id
		 * @param id id des Employee
		 * @return Employee oder NULL
		 */
		public Employee getById(long id) {
			log.trace("getById " + id);
			
			
			try(Connection conn = getConnection())
			{
				PreparedStatement stmt = conn.prepareStatement("select * from employee where id=?");
				stmt.setLong(1, id);
				ResultSet rs = stmt.executeQuery();
				return mapEmployee(rs).iterator().next();		
			}
			catch(SQLException e)
			{
				String msg = "SQL error while retreiving employee by id " + id;
				log.error(msg, e);
				throw new RepositoryException(msg);
			}
						
		}

		/**
		 * Speichert das übergebene employee in der Datenbank. UPDATE.
		 * @param i
		 */
		public void save(Employee i) {
			log.trace("save " + i);
			
			try(Connection conn = getConnection())
			{
				PreparedStatement stmt = conn.prepareStatement("update employee set name=?, firstname=?, function=?, salary=? where id=?");
				stmt.setString(1, i.getName());
				stmt.setString(2, i.getFirstname());
				stmt.setString(3, i.getFunction());
				stmt.setInt(4, i.getSalary());
				stmt.setLong(5, i.getId());
				
				stmt.executeUpdate();
			}
			catch(SQLException e)
			{
				String msg = "SQL error while updating employee " + i;
				log.error(msg , e);
				throw new RepositoryException(msg);
			}
			
		}

		/**
		 * Löscht das Employee mit der angegebenen Id von der DB
		 * @param id Employee ID
		 */
		public void delete(long id) {
			log.trace("delete " + id);
			
			try(Connection conn = getConnection())
			{
				PreparedStatement stmt = conn.prepareStatement("delete from employee where id=?");
				stmt.setLong(1, id);
				stmt.executeUpdate();
			}
			catch(SQLException e)
			{
				String msg = "SQL error while deleteing employee by id " + id;
				log.error(msg, e);
				throw new RepositoryException(msg);
			}
						

		}

		/**
		 * Speichert den angegeben Emplyee in der DB. INSERT.
		 * @param i neu zu erstellendes Employee
		 * @return Liefert die von der DB generierte id des neuen Employee zurück
		 */
		public long insert(Employee i) {
			
			log.trace("insert " + i);

			//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
			
			try(Connection conn = getConnection())
			{
				PreparedStatement stmt = conn.prepareStatement("insert into employee (name, firstname, function, salary) values (?,?,?,?)");
				stmt.setString(1, i.getName());
				stmt.setString(2, i.getFirstname());
				stmt.setString(3, i.getFunction());
				stmt.setInt(4, i.getSalary());
				stmt.executeUpdate();
				ResultSet key = stmt.getGeneratedKeys();
				key.next();
				Long id = key.getLong(1);
				return id;
			}
			catch(SQLException e)
			{
				String msg = "SQL error while updating employee " + i;
				log.error(msg , e);
				throw new RepositoryException(msg);
			}

		}
		
		/**
		 * Helper zum konvertieren der Resultsets in Employee-Objekte. Siehe getByXXX Methoden.
		 * @author Guave
		 * @throws SQLException 
		 *
		 */
		private static Collection<Employee> mapEmployee(ResultSet rs) throws SQLException 
		{
			LinkedList<Employee> list = new LinkedList<Employee>();
			while(rs.next())
			{
				Employee i = new Employee(rs.getLong("id"),rs.getString("name"),rs.getString("firstname"),rs.getString("function"),rs.getInt("salary"));
				list.add(i);
			}
			return list;
		}

	}


