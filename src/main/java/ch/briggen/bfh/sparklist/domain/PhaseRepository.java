package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Phase. 
 * Hier werden alle Funktionen für die DB-Operationen zu Phase implementiert
 * @author Guave
 *
 */


public class PhaseRepository {
	
	private final Logger log = LoggerFactory.getLogger(PhaseRepository.class);

	/**
	 * Liefert alle Phase in der Datenbank
	 * @return Collection aller Phasen
	 */
	public Collection<Phase> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select * "
					+ "from phases");
			ResultSet rs = stmt.executeQuery();
			
			return mapPhase(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all items. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * Liefert die PHase mit der übergebenen Id
	 * @param id id der Phase
	 * @return Phase oder NULL
	 */
	public Phase getById(long id) {
		log.trace("getById " + id);
		
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name from phases where id=?");
			System.out.print(stmt);
			stmt.setLong(1, id);
			System.out.print(stmt);
			ResultSet rs = stmt.executeQuery();
			return mapPhase(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving items by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	private static Collection<Phase> mapPhase(ResultSet rs) throws SQLException 
	{
		LinkedList<Phase> list = new LinkedList<Phase>();
		while(rs.next())
		{
			Phase i = new Phase(rs.getLong("id"),rs.getString("name"));
			list.add(i);
			System.out.print(list);
		}
		return list;
	}
}
