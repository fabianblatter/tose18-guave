package ch.briggen.bfh.sparklist.domain;

public class Employee {

		private long id;
		private String name;
		private String firstname;
		private String function;
		private int salary;
		
		/**
		 * Defaultkonstruktor für die Verwendung in einem Controller
		 */
		public Employee()
		{
			
		}
		
		/**
		 * Konstruktor
		 * @param id Eindeutige Id
		 * @param name Name des eintrags in der Liste
		 * @param firstname Vorname
		 * @param function Funktiosbezeichnung
		 * @param salary Lohn
		 */
		public Employee(long id, String name, String firstname, String function, int salary)
		{
			this.id = id;
			this.name = name;
			this.firstname = firstname;
			this.function = function;
			this.salary = salary;
			
		}

		public long getId() {
			return id;
		}
		
		public void setId(long id) {
			this.id = id;
		}
		
		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getFirstname() {
			return firstname;
		}
		
		public void setFirstname(String firstname) {
			this.firstname = firstname;
		}
		
		public String getFunction() {
			return function;
		}
		
		public void setFunction(String function) {
			this.function = function;
		}
		
		public int getSalary() {
			return salary;
		}
		
		public void setSalary(int salary) {
			this.salary = salary;
			
		}
		
		public String fullName() {
			return this.name + this.firstname;
		}
		
		@Override
		public String toString() {
			return String.format("Item:{id: %d; name: %s; firstname: %s; function: %s; salary: %d;}", id, name, firstname, function, salary);
		}
		

	}


