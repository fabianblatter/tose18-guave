

	package ch.briggen.bfh.sparklist.domain;


	/**
	 * 
	 * @author Guave
	 *
	 */
	public class PortfolioProjects {
		
		private long id;
		private long portfolio_id;
		private  long project_id;
		
		/**
		 * Defaultkonstruktor für die Verwendung in einem Controller
		 */
		public PortfolioProjects()
		{
			
		}
		
		/**
		 * Konstruktor
		 * @param id Eindeutige Id
		 * @param portfolioid Portfolio ids
		 * @param projectid Project ids
		 */
		public PortfolioProjects(long id, long portfolio_id, long project_id)
		{
			this.portfolio_id = portfolio_id;
			this.project_id = project_id;
			this.id = id;
		}

		

		public long getPortfolio_id() {
			return portfolio_id;
		}
		
		public void setPortfolio_id(long portfolio_id) {
			this.portfolio_id = portfolio_id;
		}
		
		public long getId() {
			return id;
		}
		public void setid(long id) {
			this.id = id;
		}
		
		public long getProject_id() {
			return project_id;
		}
		
		public void setProject_id(long project_id) {
			this.project_id = project_id;
		}
		
		
		
		@Override
		public String toString() {
			return String.format("Item:{id: %d; portfolio_id: %d; project_id: %d;}", id, portfolio_id, project_id);
		}
		

	

}
