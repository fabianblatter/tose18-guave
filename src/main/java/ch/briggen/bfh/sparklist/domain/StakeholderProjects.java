

	package ch.briggen.bfh.sparklist.domain;


	/**
	 * 
	 * @author Guave
	 *
	 */
	public class StakeholderProjects {
		
		private long id;
		private long stakeholderId;
		private  long projectId;
		
		/**
		 * Defaultkonstruktor für die Verwendung in einem Controller
		 */
		public StakeholderProjects()
		{
			
		}
		
		/**
		 * Konstruktor
		 * @param id Eindeutige Id
		 * @param stakeholderId Stakeholder-Ids
		 * @param projectId Project-Ids
		 */
		public StakeholderProjects(long id, long stakeholderId, long projectId)
		{
			this.stakeholderId = stakeholderId;
			this.projectId = projectId;
			this.id = id;
		}

		

		public long getStakeholderId() {
			return stakeholderId;
		}
		
		public void setStakeholderId(long stakeholderId) {
			this.stakeholderId = stakeholderId;
		}
		
		public long getId() {
			return id;
		}
		public void setid(long id) {
			this.id = id;
		}
		
		public long getProjectId() {
			return projectId;
		}
		
		public void setProjectId(long projectId) {
			this.projectId = projectId;
		}
		
		
		
		@Override
		public String toString() {
			return String.format("Item:{id: %d; stakeholderId: %d; projectId: %d;}", id, stakeholderId, projectId);
		}
		

	

}
