package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für die Verbindung Project und Portfolio. 
 * Hier werden alle Funktionen für die DB-Operationen zu der Verbindung Project und Portfoli implementiert
 * @author Guave
 *
 */


public class PortfolioProjectsRepository {
	
	private final Logger log = LoggerFactory.getLogger(PortfolioProjectsRepository.class);
	

	/**
	 * Liefert alle Verbindunge in der Datenbank
	 * @return Collection aller Verbindungen
	 */
	public Collection<PortfolioProjects> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, portfolio_id, project_id from portfolio_projects");
			ResultSet rs = stmt.executeQuery();
			return mapPortfolioProjects(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all items. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}


	

	/**
	 * Liefert die Verbindung mit der übergebenen Id
	 * @param id id der Verbidnugn
	 * @return Verbindung oder NULL
	 */
	public PortfolioProjects getById(long id) {
		log.trace("getById " + id);
		
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, portfolio_id, project_id from portfolio_projects where id=?");
			System.out.print(stmt);
			stmt.setLong(1, id);
			System.out.print(stmt);
			ResultSet rs = stmt.executeQuery();
			return mapPortfolioProjects(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving items by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert das übergebene Verbindung in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(PortfolioProjects i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update items set portfolio_id=?, project_id=? where id=?");
			stmt.setLong(1, i.getPortfolio_id());
			stmt.setLong(2, i.getProject_id());
			stmt.setLong(3, i.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating item " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht die Verbindung mit der angegebenen Id von der DB
	 * @param id Item ID
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from portfolio_projects where id=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing items by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert die angegebene Verbindung in der DB. INSERT.
	 * @param i neu zu erstellendes Verbindung
	 * @return Liefert die von der DB generierte id der neuen Verbindung zurück
	 */
	public long insert(PortfolioProjects i) {
		
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into portfolio_projects (portfolio_id, project_id) values (?,?)");
			stmt.setLong(1, i.getPortfolio_id());
			stmt.setLong(2, i.getProject_id());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating item " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Verbindunge-Objekte. Siehe getByXXX Methoden.
	 * @author Guave
	 * @throws SQLException 
	 *
	 */
	private static Collection<PortfolioProjects> mapPortfolioProjects(ResultSet rs) throws SQLException 
	{
		LinkedList<PortfolioProjects> list = new LinkedList<PortfolioProjects>();
		while(rs.next())
		{
			PortfolioProjects i = new PortfolioProjects(rs.getLong("id"),rs.getLong("portfolio_id"),rs.getLong("project_id"));
			list.add(i);
			System.out.print(list);
		}
		return list;
	}

}
