package ch.briggen.bfh.sparklist.domain;




	import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;
	import java.sql.Array;
	import java.sql.Connection;
	import java.sql.PreparedStatement;
	import java.sql.ResultSet;
	import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
	import org.slf4j.LoggerFactory;


	/**
	 * Repository für alle Portofolio. 
	 * Hier werden alle Funktionen für die DB-Operationen zu Porfolio implementiert
	 * @author Guave
	 *
	 */


	public class PortfolioRepository {
		
		private final Logger log = LoggerFactory.getLogger(ItemRepository.class);
		private static Map<Long, ArrayList<Long>> portfolioProjects;
		
		public PortfolioRepository() {
			portfolioProjects = getProjectIds();
		}

		/**
		 * Liefert alle Portofolio in der Datenbank
		 * @return Collection aller Portofolios
		 */
		public Collection<Portfolio> getAll()  {
			log.trace("getAll");
			try(Connection conn = getConnection())
			{
				PreparedStatement stmt = conn.prepareStatement("select * "
						+ "from portfolio");
				ResultSet rs = stmt.executeQuery();
				
				return mapPortfolio(rs);		
			}
			catch(SQLException e)
			{
				String msg = "SQL error while retreiving all items. ";
				log.error(msg, e);
				throw new RepositoryException(msg);
			}
		}


		/**
		 * Liefert das Portofolio mit der übergebenen Id
		 * @param id id des Portofolio
		 * @return Portofolio oder NULL
		 */
		public Portfolio getById(long id) {
			log.trace("getById " + id);
			
			
			try(Connection conn = getConnection())
			{
				PreparedStatement stmt = conn.prepareStatement("select * from portfolio where id=?");
				stmt.setLong(1, id);
				ResultSet rs = stmt.executeQuery();
				return mapPortfolio(rs).iterator().next();		
			}
			catch(SQLException e)
			{
				String msg = "SQL error while retreiving project by id " + id;
				log.error(msg, e);
				throw new RepositoryException(msg);
			}		
		}

		/**
		 * Speichert das übergebene Portofolio in der Datenbank. UPDATE
		 * @param i
		 */
		public void save(Portfolio portfolio) {
			log.trace("save " + portfolio);
			
			try(Connection conn = getConnection())
			{
				PreparedStatement stmt = conn.prepareStatement("update portfolio set name=?, isActive=?, Description=? where id=?");
				stmt.setString(1, portfolio.getName());
				stmt.setBoolean(2, portfolio.getIsActive());
				stmt.setString(3, portfolio.getDescription());
				
				stmt.setLong(4, portfolio.getId());
				stmt.executeUpdate();
				
				stmt = conn.prepareStatement("delete from portfolio_projects where portfolio_id=?");	
				stmt.setLong(1, portfolio.getId());				
				stmt.executeUpdate();	
				
				List<Long> projectIds = portfolio.getProjectIds();
				
				for (int i = 0; i < projectIds.size(); i++) {
					stmt = conn.prepareStatement("insert into portfolio_projects (portfolio_id, project_id) values (?, ?)");	
					stmt.setLong(1, portfolio.getId());	
					stmt.setLong(2, projectIds.get(i));
					stmt.executeUpdate();	
				}
			}
			catch(SQLException e)
			{
				String msg = "SQL error while updating project " + portfolio;
				log.error(msg , e);
				throw new RepositoryException(msg);
			}
		}

		/**
		 * Löscht das Portofolio mit der angegebenen Id von der DB
		 * @param id Portofolio ID
		 */
		public void delete(long id) {
			log.trace("delete " + id);
			
			try(Connection conn = getConnection())
			{
				PreparedStatement stmt = conn.prepareStatement("delete from Portfolio where id=?");
				stmt.setLong(1, id);
				stmt.executeUpdate();
			}
			catch(SQLException e)
			{
				String msg = "SQL error while deleteing portfolio by id " + id;
				log.error(msg, e);
				throw new RepositoryException(msg);
			}					
		}

		/**
		 * Speichert das angegebene Portofolio in der DB. INSERT.
		 * @param i neu zu erstellendes Portofolio
		 * @return Liefert die von der DB generierte id des neuen Portofolio zurück
		 */
		public long insert(Portfolio portfolio) {
			
			log.trace("insert " + portfolio);

			//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
			
			try(Connection conn = getConnection())
			{
				PreparedStatement stmt = conn.prepareStatement("insert into portfolio (name, isactive, description) values (?, ?, ?)");
				
				stmt.setString(1, portfolio.getName());
				stmt.setBoolean(2, portfolio.getIsActive());
				stmt.setString(3, portfolio.getDescription());
				
				stmt.executeUpdate();
				ResultSet key = stmt.getGeneratedKeys();
				key.next();
				Long pid = key.getLong(1);
				
				List<Long> projectIds = portfolio.getProjectIds();
				
				for (int i = 0; i < projectIds.size(); i++) {
					stmt = conn.prepareStatement("insert into portfolio_projects (portfolio_id, project_id) values (?, ?)");	
					stmt.setLong(1, pid);	
					stmt.setLong(2, projectIds.get(i));
					stmt.executeUpdate();	
				}
				
				return pid;
			}
			catch(SQLException e)
			{
				String msg = "SQL error while updating item " + portfolio;
				log.error(msg , e);
				throw new RepositoryException(msg);
			}

		}
		
		/**
		 * Helper zum konvertieren der Resultsets in Portofolio-Objekte. Siehe getByXXX Methoden.
		 * @author Guave
		 * @throws SQLException 
		 *
		 */
		private static Collection<Portfolio> mapPortfolio(ResultSet rs) throws SQLException 
		{
			LinkedList<Portfolio> listPortfolio = new LinkedList<Portfolio>();
			while(rs.next())
			{
				long pid = rs.getLong("id");
				ArrayList<Long> projectIds = getProjects(pid);
				Portfolio i = new Portfolio(pid, rs.getString("name"), rs.getBoolean("isActive"), rs.getString("description"), projectIds);
				listPortfolio.add(i);
			}
			return listPortfolio;
		}
		
		private static ArrayList<Long> getProjects(long portfolioId) {
			portfolioProjects = getProjectIds();
			if (portfolioProjects.containsKey(portfolioId)) {
				return portfolioProjects.get(portfolioId);
			} else {
				return new ArrayList<Long>();
			}
		}
		
		private static Map<Long, ArrayList<Long>> getProjectIds() {
			try(Connection conn = getConnection()) {
				PreparedStatement stmt = conn.prepareStatement("select * "
						+ "from portfolio_projects");
				ResultSet ps = stmt.executeQuery();
				
				Map<Long, ArrayList<Long>> projectIds = new HashMap<>();
				while(ps.next()) {
					Long pid = ps.getLong("portfolio_id");
					ArrayList<Long> projects = projectIds.get(pid);
					
					if (projects == null) {
						projects = new ArrayList<Long>();
					}
					projects.add(ps.getLong("project_id"));
					projectIds.put(pid, projects);
				}
				return projectIds;
				
				
			} catch(SQLException e) {
				String msg = "SQL error while getting projectIds";
				throw new RepositoryException(msg);
			}
		}
	}

