package ch.briggen.bfh.sparklist.domain;

import java.util.ArrayList;
import java.util.List;

public class Phase {

		private long id;
		private String name;
		

		/**
		 * Konstruktor
		 * @param id Eindeutige Id
		 * @param name Name der Phase
		 */
		public Phase(long id, String name) {
			this.id = id;
			this.name = name;
		}
		
		public Phase() {
			
		}


		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
		
		public String toString() {
			return this.name;
		}
	}

