package ch.briggen.bfh.sparklist.web;

	import org.slf4j.Logger;
	import org.slf4j.LoggerFactory;


import ch.briggen.bfh.sparklist.domain.Stakeholder;
import spark.Request;

	class StakeholderWebHelper {
		@SuppressWarnings("unused")
		private final static Logger log = LoggerFactory.getLogger(StakeholderWebHelper.class);
		
		public static Stakeholder StakeholderFromWeb(Request request)
		{
			return new Stakeholder(
					Long.parseLong(request.queryParams("StakeholderDetail.id")),
					request.queryParams("StakeholderDetail.stakeholder"),
					request.queryParams("StakeholderDetail.name"), 
					request.queryParams("StakeholderDetail.vorname"), 
					request.queryParams("StakeholderDetail.email"),
					Long.parseLong(request.queryParams("StakeholderDetail.einfluss")),
					Long.parseLong(request.queryParams("StakeholderDetail.interesse")));
		}

	}


