	package ch.briggen.bfh.sparklist.web;

	import java.util.HashMap;

	import org.slf4j.Logger;
	import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Employee;
import ch.briggen.bfh.sparklist.domain.EmployeeRepository;
	import ch.briggen.bfh.sparklist.domain.ItemRepository;
	import spark.ModelAndView;
	import spark.Request;
	import spark.Response;
	import spark.TemplateViewRoute;

	/**
	 * Controller für alle Operationen auf einzelnen Employees
	 * 
	 * @author Guave
	 *
	 */

	public class EmployeeEditController implements TemplateViewRoute{
		
		private final Logger log = LoggerFactory.getLogger(EmployeeEditController.class);
		
		
		
		private EmployeeRepository employeeRepo = new EmployeeRepository();
		
		
		/**
		 * Requesthandler zum Bearbeiten eines Employees. 
		 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
		 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Employee erstellt (Aufruf von /item/new)
		 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars das Employee mit der übergebenen id upgedated (Aufruf /item/save)
		 * Hört auf GET /Employee
		 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "EmployeeDetailTemplate" .
		 */
		
		@Override
		public ModelAndView handle(Request request, Response response) throws Exception {
			String idString = request.queryParams("id");
			HashMap<String, Object> model = new HashMap<String, Object>();
					
			//TODO: check if 0 or null
			if(null == idString)
			{
				log.trace("GET /employee für INSERT mit id " + idString);
				//der Submit-Button ruft /employee/new auf --> INSERT
				model.put("postAction", "/employee/new");
				model.put("employeeDetail", new Employee());

			}
			else
			{
				log.trace("GET /employee für UPDATE mit id " + idString);
				//der Submit-Button ruft /employee/update auf --> UPDATE
				model.put("postAction", "/employee/update");
				
				//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
				//dem Modell unter dem Namen "employeeDetail" hinzugefügt. itemDetal muss dem im HTML-Template verwendeten Namen entsprechen 
				Long id = Long.parseLong(idString);
				Employee i = employeeRepo.getById(id);
				model.put("employeeDetail", i);
				model.put("employeelist", employeeRepo.getAll());
			}
			
			//das Template employeeDetail verwenden und dann "anzeigen".
			return new ModelAndView(model, "employeeDetailTemplate");
		}
		
		
		
	}




