package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import ch.briggen.bfh.sparklist.domain.StakeholderRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Stakeholders
 * 
 * @author Guave
 *
 */

public class StakeholderDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(StakeholderDeleteController.class);
		
	private StakeholderRepository stakeholderRepo = new StakeholderRepository();
	

	/**
	 * Löscht das Stakeholder mit der übergebenen id in der Datenbank
	 * /Stakeholder/delete&id=987 löscht das Item mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /Stakeholders/delete (besser wäre POST)
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /stakeholder/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		stakeholderRepo.delete(longId);
		response.redirect("/");
		return null;
	}
}







