	package ch.briggen.bfh.sparklist.web;

	import org.slf4j.Logger;
	import org.slf4j.LoggerFactory;

	import ch.briggen.bfh.sparklist.domain.Employee;
	import ch.briggen.bfh.sparklist.domain.EmployeeRepository;
	import spark.ModelAndView;
	import spark.Request;
	import spark.Response;
	import spark.TemplateViewRoute;

	/**
	 * Controller für alle Operationen auf einzelnen Employees
	 *
	 * @author Guave
	 *
	 */

	public class EmployeeUpdateController implements TemplateViewRoute  {
		
		private final Logger log = LoggerFactory.getLogger(EmployeeUpdateController.class);
			
		private EmployeeRepository employeeRepo = new EmployeeRepository();
		


		/**
		 * Schreibt das geänderte Employee zurück in die Datenbank
		 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/Employee) mit der Employee-id als Parameter mit dem namen id.
		 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
		 * 
		 * Hört auf POST /Employee/update
		 * 
		 * @return redirect nach /Employee: via Browser wird /Employee aufgerufen, also editEmployee weiter oben und dann das Detailformular angezeigt.
		 */
		
		@Override
		public ModelAndView handle(Request request, Response response) throws Exception {
			Employee employeeDetail = EmployeeWebHelper.employeeFromWeb(request);
			
			log.trace("POST /employee/update mit itemDetail " + employeeDetail);
			
			//Speichern des Employee in dann den Parameter für den Redirect abfüllen
			//der Redirect erfolgt dann z.B. auf /Employee&id=3 (wenn EmployeeDetail.getId == 3 war)
			employeeRepo.save(employeeDetail);
			response.redirect("/employee?id="+employeeDetail.getId());
			return null;
		}
	}




