package ch.briggen.bfh.sparklist.web;


	import org.slf4j.Logger;
	import org.slf4j.LoggerFactory;

	import ch.briggen.bfh.sparklist.domain.Project;
	import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import spark.ModelAndView;
	import spark.Request;
	import spark.Response;
	import spark.TemplateViewRoute;

	/**
	 * Controller für alle Operationen auf einzelnen Projects
	 * 
	 * @author Guave
	 *
	 */

	public class ProjectUpdateController implements TemplateViewRoute  {
		
		private final Logger log = LoggerFactory.getLogger(ProjectUpdateController.class);
			
		private ProjectRepository ProjectRepo = new ProjectRepository();
		


		/**
		 * Schreibt das geänderte Project zurück in die Datenbank
		 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/Project) mit der Project-id als Parameter mit dem namen id.
		 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
		 * 
		 * Hört auf POST /Project/update
		 * 
		 * @return redirect nach /Project: via Browser wird /Project aufgerufen, also editItem weiter oben und dann das Detailformular angezeigt.
		 */
		
		@Override
		public ModelAndView handle(Request request, Response response) throws Exception {
			Project ProjectDetail = ProjectWebHelper.ProjectFromWeb(request);
			
			log.trace("POST /project/update mit projectDetail " + ProjectDetail);
			
			//Speichern des Projects in dann den Parameter für den Redirect abfüllen
			//der Redirect erfolgt dann z.B. auf /Project&id=3 (wenn ProjectDetail.getId == 3 war)
			ProjectRepo.save(ProjectDetail);
			response.redirect("/project?id="+ProjectDetail.getId());
			return null;
		}
	}




