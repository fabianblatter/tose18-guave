	package ch.briggen.bfh.sparklist.web;
	import org.slf4j.Logger;
	import org.slf4j.LoggerFactory;

	import ch.briggen.bfh.sparklist.domain.EmployeeRepository;
	import spark.ModelAndView;
	import spark.Request;
	import spark.Response;
	import spark.TemplateViewRoute; 

	/**
	 * Controller für alle Operationen auf einzelnen Employees
	 * 
	 * @author Guave
	 *
	 */

	public class EmployeeDeleteController implements TemplateViewRoute {
		
		private final Logger log = LoggerFactory.getLogger(ItemDeleteController.class);
			
		private EmployeeRepository employeeRepo = new EmployeeRepository();
		

		/**
		 * Löscht den Employee mit der übergebenen id in der Datenbank
		 * /item/delete&id=987 löscht den Employee mit der Id 987 aus der Datenbank
		 * 
		 * Hört auf GET /Employee/delete (besser wäre POST)
		 * 
		 * @return Redirect zurück zur Liste
		 */	
		@Override
		public ModelAndView handle(Request request, Response response) throws Exception {
			String id = request.queryParams("id");
			log.trace("GET /employee/delete mit id " + id);
			
			Long longId = Long.parseLong(id);
			employeeRepo.delete(longId);
			response.redirect("/");
			return null;
		}
	}




