package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import ch.briggen.bfh.sparklist.domain.Portfolio;
import ch.briggen.bfh.sparklist.domain.PortfolioRepository;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.StakeholderRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Portofolios
 *
 * @author Guave
 *
 */

public class PortfolioEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(PortfolioEditController.class);
	
	
	
	private PortfolioRepository portfolioRepo = new PortfolioRepository();
	private ProjectRepository projectRepo = new ProjectRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten eines Portofolios. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Portofolio erstellt (Aufruf von /item/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars das Portofolio mit der übergebenen id upgedated (Aufruf /item/save)
	 * Hört auf GET /Portofolio
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "PortofolioDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("projectList", projectRepo.getAll());
		model.put("portfoliolist", portfolioRepo.getAll());
		
				
		//TODO: check if 0 or null
		if(null == idString)
		{
			log.trace("GET /portfolio für INSERT mit id " + idString);
			//der Submit-Button ruft /portfolio/new auf --> INSERT
			model.put("postAction", "/portfolio/new");
			model.put("portfolioDetail", new Portfolio());
		} else {
			log.trace("GET /portfolio für UPDATE mit id " + idString);
			//der Submit-Button ruft /portfolio/update auf --> UPDATE
			model.put("postAction", "/portfolio/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "PortofolioDetail" hinzugefügt. itemDetal muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			Portfolio i = portfolioRepo.getById(id);
			model.put("portfolioDetail", i);
		}
		
		//das Template PortofolioDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "portfolioDetailTemplate");
	}
	
	
	
}


