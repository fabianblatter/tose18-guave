package ch.briggen.bfh.sparklist.web;


	import java.util.HashMap;

	import org.slf4j.Logger;
	import org.slf4j.LoggerFactory;
import ch.briggen.bfh.sparklist.domain.Stakeholder;
import ch.briggen.bfh.sparklist.domain.StakeholderRepository;
import spark.ModelAndView;
	import spark.Request;
	import spark.Response;
	import spark.TemplateViewRoute;

	/**
	 * Controller für alle Operationen auf einzelnen Stakeholders
	 *
	 * @author Guave
	 *
	 */

	public class StakeholderEditController implements TemplateViewRoute{
		
		private final Logger log = LoggerFactory.getLogger(StakeholderEditController.class);
		
		
		
		private StakeholderRepository stakeholderRepo = new StakeholderRepository();
		
		
		/**
		 * Requesthandler zum Bearbeiten eines Items. 
		 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
		 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Item erstellt (Aufruf von /Stakeholder/new)
		 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars das Stakeholder mit der übergebenen id upgedated (Aufruf /item/save)
		 * Hört auf GET /Stakeholder
		 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "StakeholderDetailTemplate" .
		 */
		
		@Override
		public ModelAndView handle(Request request, Response response) throws Exception {
			String idString = request.queryParams("id");
			HashMap<String, Object> model = new HashMap<String, Object>();
					
			//TODO: check if 0 or null
			if(null == idString)
			{
				//der Submit-Button ruft /stakeholder/new auf --> INSERT
				model.put("postAction", "/stakeholder/new");
				model.put("StakeholderDetail", new Stakeholder());
				model.put("stakeholderlist", stakeholderRepo.getAll());

			}
			else
			{
				log.trace("GET /stakeholder für UPDATE mit id " + idString);
				//der Submit-Button ruft /stakeholder/update auf --> UPDATE
				model.put("postAction", "/stakeholder/update");
				
				//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
				//dem Modell unter dem Namen "StakeholderDetail" hinzugefügt. StakeholderDetal muss dem im HTML-Template verwendeten Namen entsprechen 
				Long id = Long.parseLong(idString);
				Stakeholder i = stakeholderRepo.getById(id);
				model.put("StakeholderDetail", i);
				model.put("stakeholderlist", stakeholderRepo.getAll());
			}
			
			//das Template StakeholderDetail verwenden und dann "anzeigen".
			return new ModelAndView(model, "StakeholderDetailTemplate");
		}
		
		
		
	}




