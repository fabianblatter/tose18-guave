package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Portfolio;
import ch.briggen.bfh.sparklist.domain.PortfolioRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Portofolios
 *
 * @author Guave
 *
 */

public class PortfolioUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(PortfolioUpdateController.class);
		
	private PortfolioRepository portfolioRepo = new PortfolioRepository();
	


	/**
	 * Schreibt das geänderte Portofolio zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/Portofolio) mit der Portofolio-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /Portofolio/update
	 * 
	 * @return redirect nach /Portofolio: via Browser wird /Portofolio aufgerufen, also editItem weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Portfolio portfolioDetail = PortfolioWebHelper.portfolioFromWeb(request);
		
		log.trace("POST /portfolio/update mit itemDetail " + portfolioDetail);
		
		//Speichern des Portofolios in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /Portofolio&id=3 (wenn PortofolioDetail.getId == 3 war)
		portfolioRepo.save(portfolioDetail);
		response.redirect("/portfolio?id="+portfolioDetail.getId());
		return null;
	}
}


