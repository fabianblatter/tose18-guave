package ch.briggen.bfh.sparklist.web;


	import org.slf4j.Logger;
	import org.slf4j.LoggerFactory;

	import ch.briggen.bfh.sparklist.domain.Stakeholder;
	import ch.briggen.bfh.sparklist.domain.StakeholderRepository;
	import spark.ModelAndView;
	import spark.Request;
	import spark.Response;
	import spark.TemplateViewRoute;

	/**
	 * Controller für alle Operationen auf einzelnen Stakeholders
	 * 
	 * @author Guave
	 *
	 */

	public class StakeholderUpdateController implements TemplateViewRoute  {
		
		private final Logger log = LoggerFactory.getLogger(StakeholderUpdateController.class);
			
		private StakeholderRepository StakeholderRepo = new StakeholderRepository();
		


		/**
		 * Schreibt das geänderte Item zurück in die Datenbank
		 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/v) mit der Stakeholder-id als Parameter mit dem namen id.
		 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
		 * 
		 * Hört auf POST /Stakeholder/update
		 * sss
		 * @return redirect nach /Stakeholder: via Browser wird /Stakeholder aufgerufen, also editItem weiter oben und dann das Detailformular angezeigt.
		 */
		
		@Override
		public ModelAndView handle(Request request, Response response) throws Exception {
			Stakeholder StakeholderDetail = StakeholderWebHelper.StakeholderFromWeb(request);
			
			log.trace("POST /stakeholder/update mit stakeholderDetail " + StakeholderDetail);
			
			//Speichern des Stakeholders in dann den Parameter für den Redirect abfüllen
			//der Redirect erfolgt dann z.B. auf /Stakeholder&id=3 (wenn StakeholdersDetail.getId == 3 war)
			StakeholderRepo.save(StakeholderDetail);
			response.redirect("/stakeholder?id="+StakeholderDetail.getId());
			return null;
		}
	}




