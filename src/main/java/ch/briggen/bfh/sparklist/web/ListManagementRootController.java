package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import static spark.Spark.*;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.EmployeeRepository;
import ch.briggen.bfh.sparklist.domain.Item;
import ch.briggen.bfh.sparklist.domain.ItemRepository;
import ch.briggen.bfh.sparklist.domain.PortfolioRepository;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.StakeholderRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * WWW-Controller
 * Liefert unter "/" die ganze Liste
 * 
 * 
 * 
 * @author Guave
 *
 */
public class ListManagementRootController implements TemplateViewRoute {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(ListManagementRootController.class);

	ItemRepository repository = new ItemRepository();
	ProjectRepository repositoryproject = new ProjectRepository();
	PortfolioRepository repositoryportfolio = new PortfolioRepository();
	StakeholderRepository repositorystakeholder = new StakeholderRepository();
	EmployeeRepository repositoryemployee = new EmployeeRepository();
	

	/**
	 *Liefert die Liste als Root-Seite "/" zurück 
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		
		//Objekte werden geladen und die Collection dann für das Template unter dem namen "list" bereitgestellt
		//Das Template muss dann auch den Namen "list" verwenden.
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("list", repository.getAll());
		model.put("projectlist", repositoryproject.getAll());
		model.put("portfoliolist", repositoryportfolio.getAll());
		model.put("stakeholderlist", repositorystakeholder.getAll());
		model.put("employeelist", repositoryemployee.getAll());
		//if (request.url().equals("http://localhost:4567/project"))
			//return new ModelAndView(model, "projectDetailTemplate");
		//else
			return new ModelAndView(model, "listTemplate");	
	
	}
}

