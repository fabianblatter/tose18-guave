package ch.briggen.bfh.sparklist.web;

import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Portfolio;
import ch.briggen.bfh.sparklist.domain.Project;
import spark.Request;

class ProjectWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(ProjectWebHelper.class);

	public static Project ProjectFromWeb(Request request)
		{
		
			String[] sids = request.queryParamsValues("projectDetail.stakeholderIds[]");
			
			ArrayList<Long> stakeholderIds = new ArrayList<>();
			
			for (int i = 0; i < sids.length; i++) {
				long pid = Long.parseLong(sids[i]);
				stakeholderIds.add(pid);
			}
					
			String[] eids = request.queryParamsValues("projectDetail.employeeIds[]");
				
			ArrayList<Long> employeeIds = new ArrayList<>();
				
			for (int e = 0; e < eids.length; e++) {
				long pid = Long.parseLong(eids[e]);
				employeeIds.add(pid);
			}
				
			return new Project(
					Long.parseLong(request.queryParams("projectDetail.id")),
					request.queryParams("projectDetail.name"), 
					request.queryParams("projectDetail.isActive") != null,
					request.queryParams("projectDetail.owner"),
					Long.parseLong(request.queryParams("projectDetail.budget")),
					Long.parseLong(request.queryParams("projectDetail.plannedvalue")),
					Long.parseLong(request.queryParams("projectDetail.actualcosts")),
					request.queryParams("projectDetail.milestone"),
					request.queryParams("projectDetail.description"),
					Long.parseLong(request.queryParams("projectDetail.risk")),
					stakeholderIds,
					employeeIds, 
					Integer.parseInt(request.queryParams("projectDetail.phaseId"))
			);
		}
			
			
		
		}
