package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import ch.briggen.bfh.sparklist.domain.Portfolio;
import ch.briggen.bfh.sparklist.domain.PortfolioRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Portofolios
 * 
 * @author Guave
 *
 */

public class PortfolioNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(PortfolioNewController.class);
		
	private PortfolioRepository portfolioRepo = new PortfolioRepository();
	
	/**
	 * Erstellt ein neues Portofolio in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /Portofolio&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /portfolio/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Portfolio portfolioDetail = PortfolioWebHelper.portfolioFromWeb(request);
		log.trace("POST /item/new mit itemDetail " + portfolioDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long id = portfolioRepo.insert(portfolioDetail);
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /Portofolio?id=432932
		response.redirect("/portfolio?id="+id);
		return null;
	}
}


