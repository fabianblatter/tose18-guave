package ch.briggen.bfh.sparklist.web;


	import java.util.HashMap;

	import org.slf4j.Logger;
	import org.slf4j.LoggerFactory;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.EmployeeRepository;
import ch.briggen.bfh.sparklist.domain.PhaseRepository;
import ch.briggen.bfh.sparklist.domain.StakeholderRepository;
import spark.ModelAndView;
	import spark.Request;
	import spark.Response;
	import spark.TemplateViewRoute;

	/**
	 * Controller für alle Operationen auf einzelnen Projects
	 * 
	 * @author Guave
	 *
	 */

	public class ProjectEditController implements TemplateViewRoute{
		
		private final Logger log = LoggerFactory.getLogger(ProjectEditController.class);
		
		private ProjectRepository projectRepo = new ProjectRepository();
		private StakeholderRepository stakeholderRepo = new StakeholderRepository();
		private EmployeeRepository employeeRepo = new EmployeeRepository();
		private PhaseRepository phaseRepo = new PhaseRepository();
		
		
		/**
		 * Requesthandler zum Bearbeiten eines Projects. 
		 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
		 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Project erstellt (Aufruf von /item/new)
		 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars das Item mit der übergebenen id upgedated (Aufruf /item/save)
		 * Hört auf GET /Project
		 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "ProjectDetailTemplate" .
		 */
		
		@Override
		public ModelAndView handle(Request request, Response response) throws Exception {
			String idString = request.queryParams("id");
			HashMap<String, Object> model = new HashMap<String, Object>();
			model.put("stakeholderList", stakeholderRepo.getAll());
			model.put("employeeList", employeeRepo.getAll());
			model.put("projectList", projectRepo.getAll());
			model.put("phaseList", phaseRepo.getAll());
					
			//TODO: check if 0 or null
			if(null == idString)
			{
				log.trace("GET /project für INSERT mit id " + idString);
				//der Submit-Button ruft /project/new auf --> INSERT
				model.put("postAction", "/project/new");
				model.put("projectDetail", new Project());
			} else {
				log.trace("GET /project für UPDATE mit id " + idString);
				//der Submit-Button ruft /project/update auf --> UPDATE
				model.put("postAction", "/project/update");
				
				//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
				//dem Modell unter dem Namen "projectDetail" hinzugefügt. ProjectDetal muss dem im HTML-Template verwendeten Namen entsprechen 
				Long id = Long.parseLong(idString);
				Project i = projectRepo.getById(id);
				model.put("projectDetail", i);
				
			}
			
			//das Template projectDetail verwenden und dann "anzeigen".
			return new ModelAndView(model, "projectDetailTemplate");
		}
		
		
		
	}