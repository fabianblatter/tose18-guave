package ch.briggen.bfh.sparklist.web;



	import org.slf4j.Logger;
	import org.slf4j.LoggerFactory;



import ch.briggen.bfh.sparklist.domain.Stakeholder;
import ch.briggen.bfh.sparklist.domain.StakeholderRepository;
import spark.ModelAndView;
	import spark.Request;
	import spark.Response;
	import spark.TemplateViewRoute; 

	/**
	 * Controller für alle Operationen auf einzelnen Stakeholder
	 * @author Guave
	 *
	 */

	public class StakeholderNewController implements TemplateViewRoute {
		
		private final Logger log = LoggerFactory.getLogger(StakeholderNewController.class);
			
		private StakeholderRepository stakeholderRepo = new StakeholderRepository();
		
		/**
		 * Erstellt ein neues Stakeholder in der DB. Die id wird von der Datenbank erstellt.
		 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /stakeholder&id=99  wenn die id 99 war.)
		 * 
		 * Hört auf POST /stakeholder/new
		 * 
		 * @return Redirect zurück zur Detailmaske
		 */	
		public ModelAndView handle(Request request, Response response) throws Exception {
			Stakeholder StakeholderDetail = StakeholderWebHelper.StakeholderFromWeb(request);
			log.trace("POST /stakeholder/new mit StakeholderDetail" + StakeholderDetail);
			
			//insert gibt die von der DB erstellte id zurück.
			long id = stakeholderRepo.insert(StakeholderDetail);
			
			//die neue Id wird dem Redirect als Parameter hinzugefügt
			//der redirect erfolgt dann auf /stakeholder?id=432932
			response.redirect("/stakeholder?id="+id);
			return null;
		}
		
	}




