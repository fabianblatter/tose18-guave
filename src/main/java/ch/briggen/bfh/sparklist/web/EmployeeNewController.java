	package ch.briggen.bfh.sparklist.web;

	import org.slf4j.Logger;
	import org.slf4j.LoggerFactory;

	import ch.briggen.bfh.sparklist.domain.Employee;
	import ch.briggen.bfh.sparklist.domain.EmployeeRepository;
	import ch.briggen.bfh.sparklist.domain.ItemRepository;
	import spark.ModelAndView;
	import spark.Request;
	import spark.Response;
	import spark.TemplateViewRoute; 

	/**
	 * Controller für alle Operationen auf einzelnen Employees
	 *
	 * @author Guave
	 *
	 */

	public class EmployeeNewController implements TemplateViewRoute {
		
		private final Logger log = LoggerFactory.getLogger(EmployeeNewController.class);
			
		private EmployeeRepository employeeRepo = new EmployeeRepository();
		
		/**
		 * Erstellt ein neues Employee in der DB. Die id wird von der Datenbank erstellt.
		 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /Employee&id=99  wenn die id 99 war.)
		 * 
		 * Hört auf POST /Employee/new
		 * 
		 * @return Redirect zurück zur Detailmaske
		 */	
		@Override
		public ModelAndView handle(Request request, Response response) throws Exception {
			Employee employeeDetail = EmployeeWebHelper.employeeFromWeb(request);
			log.trace("POST /item/new mit employeeDetail " + employeeDetail);
			
			//insert gibt die von der DB erstellte id zurück.
			long id = employeeRepo.insert(employeeDetail);
			
			//die neue Id wird dem Redirect als Parameter hinzugefügt
			//der redirect erfolgt dann auf /Employee?id=432932
			response.redirect("/employee?id="+id);
			return null;
		}
	}




