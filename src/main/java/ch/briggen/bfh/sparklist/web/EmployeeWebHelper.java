	package ch.briggen.bfh.sparklist.web;



	import org.slf4j.Logger;
	import org.slf4j.LoggerFactory;

	import ch.briggen.bfh.sparklist.domain.Employee;
	import spark.Request;

	class EmployeeWebHelper {
		@SuppressWarnings("unused")
		private final static Logger log = LoggerFactory.getLogger(EmployeeWebHelper.class);
		
		public static Employee employeeFromWeb(Request request)
		{
			return new Employee(
					Long.parseLong(request.queryParams("employeeDetail.id")),
					request.queryParams("employeeDetail.name"),
					request.queryParams("employeeDetail.firstname"),
					request.queryParams("employeeDetail.function"),
					Integer.parseInt(request.queryParams("employeeDetail.salary")));
		}

	}


