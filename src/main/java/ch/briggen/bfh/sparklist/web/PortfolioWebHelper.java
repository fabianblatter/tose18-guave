package ch.briggen.bfh.sparklist.web;



import java.sql.Array;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ch.briggen.bfh.sparklist.domain.Portfolio;
import ch.briggen.bfh.sparklist.domain.Project;
import spark.Request;

class PortfolioWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(PortfolioWebHelper.class);
	
	@SuppressWarnings("unchecked")
	public static Portfolio portfolioFromWeb(Request request) {	
		String[] pids = request.queryParamsValues("portfolioDetail.projectIds[]");
		
		ArrayList<Long> projectIds = new ArrayList<>();
		
		for (int i = 0; i < pids.length; i++) {
			long pid = Long.parseLong(pids[i]);
			projectIds.add(pid);
		}
		
		return new Portfolio(
				Long.parseLong(request.queryParams("portfolioDetail.id")),
				request.queryParams("portfolioDetail.name"),
				request.queryParams("portfolioDetail.isActive") != null,
				request.queryParams("portfolioDetail.description"),
				projectIds
		);
	}

}