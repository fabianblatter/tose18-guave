package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import ch.briggen.bfh.sparklist.domain.PortfolioRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Portofolios
 *
 * @author Guave
 *
 */

public class PortfolioDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(PortfolioDeleteController.class);
		
	private PortfolioRepository portfolioRepo = new PortfolioRepository();
	

	/**
	 * Löscht das Portofolio mit der übergebenen id in der Datenbank
	 * /Portofolio/delete&id=987 löscht das Portofolio mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /Portofolio/delete (besser wäre POST)
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /portfolio/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		portfolioRepo.delete(longId);
		response.redirect("/");
		return null;
	}
}


