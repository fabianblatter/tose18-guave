package ch.briggen.bfh.sparklist;

import static spark.Spark.get;
import static spark.Spark.post;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.web.EmployeeDeleteController;
import ch.briggen.bfh.sparklist.web.EmployeeEditController;
import ch.briggen.bfh.sparklist.web.EmployeeNewController;
import ch.briggen.bfh.sparklist.web.EmployeeUpdateController;
import ch.briggen.bfh.sparklist.web.ItemDeleteController;
import ch.briggen.bfh.sparklist.web.ItemEditController;
import ch.briggen.bfh.sparklist.web.ItemNewController;
import ch.briggen.bfh.sparklist.web.ItemUpdateController;
import ch.briggen.bfh.sparklist.web.ListManagementRootController;
import ch.briggen.bfh.sparklist.web.PortfolioDeleteController;
import ch.briggen.bfh.sparklist.web.PortfolioEditController;
import ch.briggen.bfh.sparklist.web.PortfolioNewController;
import ch.briggen.bfh.sparklist.web.PortfolioUpdateController;
import ch.briggen.bfh.sparklist.web.ProjectDeleteController;
import ch.briggen.bfh.sparklist.web.ProjectEditController;
import ch.briggen.bfh.sparklist.web.ProjectNewController;
import ch.briggen.bfh.sparklist.web.ProjectUpdateController;
import ch.briggen.bfh.sparklist.web.StakeholderDeleteController;
import ch.briggen.bfh.sparklist.web.StakeholderEditController;
import ch.briggen.bfh.sparklist.web.StakeholderNewController;
import ch.briggen.bfh.sparklist.web.StakeholderUpdateController;
import ch.briggen.sparkbase.H2SparkApp;
import ch.briggen.sparkbase.UTF8ThymeleafTemplateEngine;

public class SparkListServer extends H2SparkApp {

	final static Logger log = LoggerFactory.getLogger(SparkListServer.class);

	public static void main(String[] args) {

		SparkListServer server = new SparkListServer();
		server.configure();
		server.run();
	}

	@Override
	protected void doConfigureHttpHandlers() {
	
		get("/", new ListManagementRootController(), new UTF8ThymeleafTemplateEngine());
		get("/item", new ItemEditController(), new UTF8ThymeleafTemplateEngine());
		post("/item/update", new ItemUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/item/delete", new ItemDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/item/new", new ItemNewController(), new UTF8ThymeleafTemplateEngine());
		get("/project", new ProjectEditController(), new UTF8ThymeleafTemplateEngine());
		post("/project/update", new ProjectUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/project/delete", new ProjectDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/project/new", new ProjectNewController(), new UTF8ThymeleafTemplateEngine());
		get("/employee", new EmployeeEditController(), new UTF8ThymeleafTemplateEngine());
		post("/employee/update", new EmployeeUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/employee/delete", new EmployeeDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/employee/new", new EmployeeNewController(), new UTF8ThymeleafTemplateEngine());
		get("/portfolio", new PortfolioEditController(), new UTF8ThymeleafTemplateEngine());
		post("/portfolio/update", new PortfolioUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/portfolio/delete", new PortfolioDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/portfolio/new", new PortfolioNewController(), new UTF8ThymeleafTemplateEngine());
		get("/stakeholder", new StakeholderEditController(), new UTF8ThymeleafTemplateEngine());
		post("/stakeholder/update", new StakeholderUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/stakeholder/delete", new StakeholderDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/stakeholder/new", new StakeholderNewController(), new UTF8ThymeleafTemplateEngine());	
	}

}
