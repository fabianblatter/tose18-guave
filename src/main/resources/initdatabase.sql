create table if not exists items (id long identity, name varchar(45), quantity decimal);
create table if not exists project (id long identity,  name varchar(45), isactive varchar(45), owner varchar(45), budget long, plannedvalue long, actualcosts long, milestone varchar(45), stakeholder varchar(45), description varchar(200), risk long, phaseId long);
create table if not exists employee (id long identity, name varchar(45), firstname varchar(45), function varchar(45), salary long);
create table if not exists stakeholder (id long identity, stakeholder varchar(45), name varchar(45), vorname varchar(45), email varchar(100), einfluss long, interesse long);
create table if not exists portfolio (id long identity, name varchar(45), isactive varchar(45), owner varchar(45), budget long, plannedvalue long, actualcosts long, milestone varchar(200), description varchar(200));
create table if not exists portfolio_projects (id long identity, portfolio_id long, project_id long);
create table if not exists employee_projects (id long identity, employee_id long, project_id long);
create table if not exists stakeholder_projects (id long identity, stakeholder_id long, project_id long);

create table if not exists phases (id long identity, name varchar(45));
replace into phases (id, name) values (0, 'Initialisierungsphase'); 
replace into phases (id, name) values (1, 'Konzeptionsphase'); 
replace into phases (id, name) values (2, 'Realisierungsphase'); 
replace into phases (id, name) values (3, 'Einführungsphase'); 