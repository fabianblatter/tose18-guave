package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class ProjectTest {

	@Test
	void testEmptyContructorYieldsEmptyObject() {
		Project p = new Project();
		assertThat("Id",p.getId(),is(equalTo(0l)));
		assertThat("Name",p.getName(),is(equalTo(null)));
		assertThat("Status",p.getIsActive(),is(equalTo(true)));
		assertThat("Owner",p.getOwner(),is(equalTo(null)));
		assertThat("Budget",p.getBudget(),is(equalTo(1l)));
		assertThat("PlannedValue",p.getPlannedValue(),is(equalTo(1l)));
		assertThat("ActualCosts",p.getActualCosts(),is(equalTo(1l)));
		assertThat("Milestone",p.getMilestone(),is(equalTo("")));
		assertThat("Description",p.getDescription(),is(equalTo("")));
	}

	
	@ParameterizedTest
	@CsvSource({"1,One, true, Bla, 2, 1, 3, Bli, Blub, 2, 1"})
	void testContructorAssignsAllFields(long id,String name, boolean IsActive,  String Owner, long Budget, long PlannedValue, long ActualCosts, String Milestone, String Description, long risk, int phase) {
		List<Long> stakeholderIds = new ArrayList<>();
		stakeholderIds.add(1l);
		stakeholderIds.add(2l);
		stakeholderIds.add(12l);
		
		List<Long> employeeIds = new ArrayList<>();
		stakeholderIds.add(3l);
		stakeholderIds.add(5l);
		stakeholderIds.add(9l);
		
		Project p = new Project(id, name, IsActive, Owner, Budget, PlannedValue, ActualCosts, Milestone, Description, risk, stakeholderIds, employeeIds, phase);
		assertThat("Id",p.getId(),equalTo(id));
		assertThat("Name",p.getName(),equalTo(name));
		assertThat("Status",p.getIsActive(),equalTo(IsActive));
		assertThat("Owner",p.getOwner(),equalTo(Owner));
		assertThat("Busget",p.getBudget(),equalTo(Budget));
		assertThat("PlannedValue",p.getPlannedValue(),equalTo(PlannedValue));
		assertThat("ActualCosts",p.getActualCosts(),equalTo(ActualCosts));
		assertThat("Milestone",p.getMilestone(),equalTo(Milestone));
		assertThat("Description",p.getDescription(),equalTo(Description));
		assertThat("risk",p.getRisk(),equalTo(risk));
		assertThat("StakeholerIds",p.getStakeholderIds(),equalTo(stakeholderIds));
		assertThat("employeeIds",p.getEmployeeIds(),equalTo(employeeIds));
	}


	
	@Test
	void testToString() {
		Project p = new Project();
		System.out.println(p.toString());
		assertThat("toString smoke test",p.toString(),not(nullValue()));
	}

}
