package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDB;
import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDataSourceForTest;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class ProjectRepositoryTest {
	ProjectRepository repo = null;

	
	private static void populateRepo(ProjectRepository r) {
		for(int i = 0; i <10; ++i) {
			List<Long> stakeholderIds = new ArrayList<>();
			stakeholderIds.add(1l * i);
			stakeholderIds.add(2l * i);
			stakeholderIds.add(12l * i);
			
			List<Long> employeeIds = new ArrayList<>();
			stakeholderIds.add(2l * i);
			stakeholderIds.add(4l * i);
			stakeholderIds.add(11l * i);
			
			Project dummy = new Project(i, "Fake Project" + i, true, "Fake Owner" + i, i * 10000, i * 15000, i * 12500, "Fake Milestone" + i, "Fake Description" + i, i * 2l,stakeholderIds, employeeIds, 0);
			r.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		repo = new ProjectRepository();
	}

	@Test
	void testEmptyDB() {
		assertThat("New DB must be empty",repo.getAll().isEmpty(),is(true));
	}
	
	@Test
	void testPopulatedDB()
	{
		populateRepo(repo);
		assertThat("Freshly populated DB must hold 10 ites",repo.getAll().size(),is(10));
	}
		
	@ParameterizedTest
	@CsvSource({"1,One, true, Bla, 2, 1, 3, Bli, Blub, 2, 0"})
	void testInsertItems(long id,String name, boolean IsActive,  String Owner, long Budget, long PlannedValue, long ActualCosts, String Milestone, String Description, long risk, int phase)
	{
		populateRepo(repo);
		
		List<Long> stakeholderIds = new ArrayList<>();
		stakeholderIds.add(2l);
		stakeholderIds.add(3l);
		stakeholderIds.add(11l);
		
		List<Long> employeeIds = new ArrayList<>();
		stakeholderIds.add(3l);
		stakeholderIds.add(5l);
		stakeholderIds.add(9l);
		
		Project p = new Project(id, name, IsActive, Owner, Budget, PlannedValue, ActualCosts, Milestone, Description, risk, stakeholderIds, employeeIds, phase);
		long dbId = repo.insert(p);
		Project fromDB = repo.getById(dbId);
		
		assertThat("Id",p.getId(),equalTo(id));
		assertThat("Name",p.getName(),equalTo(name));
		assertThat("Status",p.getIsActive(),equalTo(IsActive));
		assertThat("Owner",p.getOwner(),equalTo(Owner));
		assertThat("Busget",p.getBudget(),equalTo(Budget));
		assertThat("PlannedValue",p.getPlannedValue(),equalTo(PlannedValue));
		assertThat("ActualCosts",p.getActualCosts(),equalTo(ActualCosts));
		assertThat("Milestone",p.getMilestone(),equalTo(Milestone));
		assertThat("Description",p.getDescription(),equalTo(Description));
		assertThat("StakeholerIds",p.getStakeholderIds(),equalTo(stakeholderIds));
		assertThat("Phase",p.getPhaseId(),equalTo(phase));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One, true, Bla, 2, 1, 3, Bli, Blub, 2, 0"})
	void testUpdateItems(long id,String name, boolean IsActive,  String Owner, long Budget, long PlannedValue, long ActualCosts, String Milestone, String Description, long risk, int phase)
	{
		populateRepo(repo);
		
		List<Long> stakeholderIds = new ArrayList<>();
		stakeholderIds.add(2l);
		stakeholderIds.add(3l);
		stakeholderIds.add(11l);
		
		List<Long> employeeIds = new ArrayList<>();
		stakeholderIds.add(3l);
		stakeholderIds.add(5l);
		stakeholderIds.add(9l);
		
		Project p = new Project(id, name, IsActive, Owner, Budget, PlannedValue, ActualCosts, Milestone, Description, risk, stakeholderIds, employeeIds, phase);
		assertThat("Name",p.getName(),equalTo("One"));
		assertThat("Status",p.getIsActive(),equalTo(true));
		
		long dbId = repo.insert(p);
		p.setId(dbId);
		p.setName("New Name");
		p.setIsActive(false);
		repo.save(p);
		
		Project fromDB = repo.getById(dbId);
		assertThat("Id",p.getId(),equalTo(dbId));
		assertThat("Name",p.getName(),equalTo("New Name"));
		assertThat("Status",p.getIsActive(),equalTo(false));
		assertThat("Owner",p.getOwner(),equalTo(Owner));
		assertThat("Busget",p.getBudget(),equalTo(Budget));
		assertThat("PlannedValue",p.getPlannedValue(),equalTo(PlannedValue));
		assertThat("ActualCosts",p.getActualCosts(),equalTo(ActualCosts));
		assertThat("Milestone",p.getMilestone(),equalTo(Milestone));
		assertThat("Description",p.getDescription(),equalTo(Description));
		assertThat("StakeholerIds",p.getStakeholderIds(),equalTo(stakeholderIds));	
	}
	
	@ParameterizedTest
	@CsvSource({"1,One, true, Bla, 2, 1, 3, Bli, Blub, 2, 0"})
	void testDeleteItems(long id,String name, boolean IsActive,  String Owner, long Budget, long PlannedValue, long ActualCosts, String Milestone, String Description, long risk, int phase)
	{
		populateRepo(repo);
		
		List<Long> stakeholderIds = new ArrayList<>();
		stakeholderIds.add(2l);
		stakeholderIds.add(3l);
		stakeholderIds.add(11l);
		
		List<Long> employeeIds = new ArrayList<>();
		stakeholderIds.add(3l);
		stakeholderIds.add(5l);
		stakeholderIds.add(9l);

		Project p = new Project(id, name, IsActive, Owner, Budget, PlannedValue, ActualCosts, Milestone, Description, risk, stakeholderIds, employeeIds, phase);
		long dbId = repo.insert(p);
		Project fromDB = repo.getById(dbId);
		
		assertThat("Item was written to DB",fromDB,not(nullValue()));
		repo.delete(dbId);
		assertThrows(NoSuchElementException.class, ()->{repo.getById(dbId);},"Item should have been deleted");
	}
	
	@Test
	void testDeleteManyRows()
	{
		populateRepo(repo);
		for(Project p : repo.getAll())
		{
			repo.delete(p.getId());
		}
		
		assertThat("DB must be empty after deleteing all items",repo.getAll().isEmpty(),is(true));
	}
}
